#ifndef REMOTEPICAMERA_H
#define REMOTEPICAMERA_H

#include <QObject>
#include <QJsonObject>
#include <QtNetwork>

#include "commons.h"
#include "abstractcamera.h"
#include "discovery.h"

class RemotePiCamera : public QObject
{
    Q_OBJECT
public:
    explicit RemotePiCamera(QObject *parent = nullptr);

    void loadConfiguration();
    void saveConfiguration();
    void start();

private:
    QJsonObject configuration;
    QTcpServer *server;
    QTcpSocket *client;
    Discovery *discoveryServer;
    AbstractCamera *cameraHandler;
    QDataStream incomingStream;
    QByteArray incomingData;

    void selectCameraModel();
    void initCamera();

signals:

private slots:
    void handleNewConnection();
    void readIncomingData();
};

#endif // REMOTEPICAMERA_H
