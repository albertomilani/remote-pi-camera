#include <QCoreApplication>
#include "commons.h"
#include "remotepicamera.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    RemotePiCamera rpc;

    rpc.start();

    return a.exec();
}
