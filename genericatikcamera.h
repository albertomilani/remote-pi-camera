#ifndef GENERICATIKCAMERA_H
#define GENERICATIKCAMERA_H

#include "abstractcamera.h"
#include "AtikCameras.h"

class GenericAtikCamera : public AbstractCamera
{
public:
    GenericAtikCamera(QObject *parent = nullptr);

    uint connectedCameras(uint *cameraNum);
    uint connectCamera();
    uint disconnectCamera();

protected:
    ArtemisHandle camHandler;
};

#endif // GENERICATIKCAMERA_H
