#ifndef DISCOVERY_H
#define DISCOVERY_H

#include <QThread>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>


class Discovery : public QThread
{
public:
    Discovery(quint16 sp);

private:
    quint16 serverPort;

protected:
    void run();
};

#endif // DISCOVERY_H
