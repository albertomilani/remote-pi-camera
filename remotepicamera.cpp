#include "remotepicamera.h"
#include <stdlib.h>
#include <QCoreApplication>
#include <QFile>
#include <QJsonDocument>
#include <QDebug>

#include "asi120mmcamera.h"
#include "atikhorizoniicamera.h"
#include "fakecamera.h"

RemotePiCamera::RemotePiCamera(QObject *parent) : QObject(parent)
  , server(NULL)
  , client(NULL)
  , discoveryServer(NULL)
  , cameraHandler(NULL)
{

}

void RemotePiCamera::loadConfiguration() {

    QFile configFile;

    if (QFile::exists(CONFIG_FILENAME)) {

        configFile.setFileName(CONFIG_FILENAME);
        configFile.open(QIODevice::ReadOnly|QIODevice::Text);
        configuration = QJsonDocument().fromJson(configFile.readAll()).object();
        configFile.close();

    } else {
        qDebug() << "missing config file";
        exit(1);
    }

}

void RemotePiCamera::saveConfiguration() {

    QFile configFile;

    configFile.setFileName(CONFIG_FILENAME);

    configFile.open(QIODevice::ReadWrite|QIODevice::Text);
    QJsonDocument jsonDoc;
    jsonDoc.setObject(configuration);
    configFile.write(jsonDoc.toJson());
    configFile.close();
}

void RemotePiCamera::start() {

    // read configuration
    loadConfiguration();

    // start main server
    if (server == NULL) {
        server = new QTcpServer();
    }

    quint16 serverPort = 0;
    if (configuration.value(CONFIG_KEY_SERVER_PORT) != QJsonValue::Null) {
        serverPort = (quint16)configuration.value(CONFIG_KEY_SERVER_PORT).toInt();
    }

    connect(server, &QTcpServer::newConnection, this, &RemotePiCamera::handleNewConnection);

    server->setMaxPendingConnections(1);
    server->listen(QHostAddress::Any, serverPort);

    // start discovery server
    if (discoveryServer == NULL) {
        discoveryServer = new Discovery(server->serverPort());
    }
    discoveryServer->start();

    // camera driver selection
    selectCameraModel();

    // initialize camera
    initCamera();

}

void RemotePiCamera::selectCameraModel() {

    if (configuration.value(CONFIG_KEY_CAMERA_MODEL) != QJsonValue::Null) {

        QString configCameraModel = configuration.value(CONFIG_KEY_CAMERA_MODEL).toString();

        if (QString(CONFIG_VALUE_CAMERA_ASI_ASI_120MM) == configCameraModel) {
            cameraHandler = new Asi120mmCamera();
        } else if (QString(CONFIG_VALUE_CAMERA_ATIK_HORIZON_II) == configCameraModel) {
            cameraHandler = new AtikHorizonIICamera();
        } else if (QString(CONFIG_VALUE_CAMERA_SIMULATOR) == configCameraModel) {
            cameraHandler = new FakeCamera();
        } else {
            // unsupported camera
        }
    }
}

void RemotePiCamera::initCamera() {

    if (cameraHandler != NULL) {
        // TODO check if the camera is physically connected
        cameraHandler->connectCamera();
    }
}

void RemotePiCamera::handleNewConnection() {

    if (server->hasPendingConnections()) {
        client = server->nextPendingConnection();
        connect(client, &QTcpSocket::readyRead, this, &RemotePiCamera::readIncomingData);
    }
}

void RemotePiCamera::readIncomingData() {

    incomingStream.startTransaction();

    QByteArray data;
    incomingStream >> data;

    if (!incomingStream.commitTransaction()) {
        return ;
    }

    incomingData = data;
}
