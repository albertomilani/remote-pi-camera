#include "discovery.h"

Discovery::Discovery(quint16 sp) : QThread()
{
    serverPort = sp;
}

void Discovery::run() {

    // Open a socket
    int sd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sd <= 0) {
        return;
    }

    // Set socket options
    // Enable broadcast
    int broadcastEnable=1;
    int ret = setsockopt(sd, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));
    if (ret) {
        close(sd);
        return;
    }

    // Configure the port and ip we want to send to
    struct sockaddr_in broadcastAddr; // Make an endpoint
    memset(&broadcastAddr, 0, sizeof broadcastAddr);
    broadcastAddr.sin_family = AF_INET;
    inet_pton(AF_INET, "255.255.255.255", &broadcastAddr.sin_addr); // Set the broadcast IP address
    broadcastAddr.sin_port = htons(24242);

    // Send the broadcast request
    QString message = "RemotePiCamera on port " + QString(serverPort);
    const char *request = message.toLocal8Bit().data();
    while (true) {
        ret = sendto(sd, request, strlen(request), 0, (struct sockaddr*)&broadcastAddr, sizeof broadcastAddr);
//        if (ret<0) {
//            close(sd);
//            return;
//        }
        QThread::sleep(30);
    }

    close(sd);
}
