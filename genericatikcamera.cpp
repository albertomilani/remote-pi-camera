#include "genericatikcamera.h"

GenericAtikCamera::GenericAtikCamera(QObject *parent) : AbstractCamera(parent) {

}

uint GenericAtikCamera::connectedCameras(uint *cameraNum){

    ArtemisRefreshDevicesCount();
    *cameraNum = ArtemisDeviceCount();

    return AbstractCamera::CameraReturnCodes::RPC_OK;
};

uint GenericAtikCamera::connectCamera() {

    camHandler = ArtemisConnect(-1);
    if (camHandler == NULL) {
        return AbstractCamera::CameraReturnCodes::RPC_CONNECTION_ERROR;
    }

    return AbstractCamera::CameraReturnCodes::RPC_OK;
}

uint GenericAtikCamera::disconnectCamera() {

    if (camHandler != NULL) {
        if (!ArtemisDisconnect(camHandler)) {
            return AbstractCamera::CameraReturnCodes::RPC_DISCONNECTION_ERROR;
        }
    }

    return AbstractCamera::CameraReturnCodes::RPC_OK;
}
