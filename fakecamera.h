#ifndef FAKECAMERA_H
#define FAKECAMERA_H

#include "abstractcamera.h"

class FakeCamera : public AbstractCamera
{
public:
    FakeCamera(QObject *parent = nullptr);

    uint cameraVendor(QString *vendor);
};

#endif // FAKECAMERA_H
