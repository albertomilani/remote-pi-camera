#include "genericasicamera.h"

GenericAsiCamera::GenericAsiCamera(QObject *parent) : AbstractCamera(parent) {

    cameraId = -1; // get first available camera
}

uint GenericAsiCamera::connectCamera() {

    ASIOpenCamera(cameraId);
    ASIInitCamera(cameraId);

    return AbstractCamera::CameraReturnCodes::RPC_OK;
}
