#ifndef ATIKHORIZONIICAMERA_H
#define ATIKHORIZONIICAMERA_H

#include "genericatikcamera.h"

class AtikHorizonIICamera : public GenericAtikCamera
{
public:
    AtikHorizonIICamera(QObject *parent = nullptr);
};

#endif // ATIKHORIZONIICAMERA_H
