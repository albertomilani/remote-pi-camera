#ifndef GENERICASICAMERA_H
#define GENERICASICAMERA_H

#include "abstractcamera.h"
#include "ASICamera2.h"

class GenericAsiCamera : public AbstractCamera
{
public:
    GenericAsiCamera(QObject *parent = nullptr);

    uint connectCamera();

private:
    uint cameraId;
};

#endif // GENERICASICAMERA_H
