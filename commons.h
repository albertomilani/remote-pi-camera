#ifndef COMMONS_H
#define COMMONS_H

#define DEV true

#if DEV
#define CONFIG_FILENAME "/Users/milhouse/remotepicamera.conf"
#else
#define CONFIG_FILENAME  "/home/osservatorio/remotepicamera.conf"
#endif

#define CONFIG_KEY_SERVER_PORT "serverPort"
#define CONFIG_KEY_CAMERA_MODEL "cameraModel"
#define CONFIG_VALUE_CAMERA_SIMULATOR "simulator"
#define CONFIG_VALUE_CAMERA_ATIK_HORIZON_II "atik-horizon-ii"
#define CONFIG_VALUE_CAMERA_ASI_ASI_120MM "zwo-asi-120mm"

#endif // COMMONS_H
