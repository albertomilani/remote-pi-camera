#include "fakecamera.h"

FakeCamera::FakeCamera(QObject *parent) : AbstractCamera(parent)
{

}

uint FakeCamera::cameraVendor(QString *vendor) {

    *vendor = "FakeCamera";

    return AbstractCamera::CameraReturnCodes::RPC_OK;
}
