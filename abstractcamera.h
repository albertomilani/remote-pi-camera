#ifndef ABSTRACTCAMERA_H
#define ABSTRACTCAMERA_H

#include <QObject>

class AbstractCamera : public QObject
{
    Q_OBJECT
public:
    explicit AbstractCamera(QObject *parent = nullptr) {Q_UNUSED(parent)};

    enum CameraReturnCodes {
        RPC_OK = 0,
        RPC_GENERIC_ERROR = 1,
        RPC_NOT_IMPLEMENTED = 2,
        RPC_NOT_CONNECTED = 3,
        RPC_CONNECTION_ERROR = 4,
        RPC_DISCONNECTION_ERROR = 5
    };

    virtual uint cameraVendor(QString *vendor) {
        Q_UNUSED(vendor);
        return CameraReturnCodes::RPC_NOT_IMPLEMENTED;
    };

    virtual uint connectedCameras(uint *cameraNum) {
        Q_UNUSED(cameraNum);
        return CameraReturnCodes::RPC_NOT_IMPLEMENTED;
    };

    virtual uint connectCamera() {
        return CameraReturnCodes::RPC_NOT_IMPLEMENTED;
    }

    virtual uint disconnectCamera() {
        return CameraReturnCodes::RPC_NOT_IMPLEMENTED;
    }

    virtual uint setBinning(uint xBin, uint yBin) {
        Q_UNUSED(xBin);
        Q_UNUSED(yBin);
        return CameraReturnCodes::RPC_NOT_IMPLEMENTED;
    };
    virtual uint setSubframe(uint xStart, uint yStart, uint w, uint h) {
        Q_UNUSED(xStart);
        Q_UNUSED(yStart);
        Q_UNUSED(w);
        Q_UNUSED(h);
        return CameraReturnCodes::RPC_NOT_IMPLEMENTED;
    };
    virtual uint captureImage(quint64 msec) {
        Q_UNUSED(msec);
        return CameraReturnCodes::RPC_NOT_IMPLEMENTED;
    };

signals:

};

#endif // ABSTRACTCAMERA_H
