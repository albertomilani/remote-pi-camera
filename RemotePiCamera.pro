QT -= gui
QT += network

CONFIG += c++11 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        asi120mmcamera.cpp \
        atikhorizoniicamera.cpp \
        discovery.cpp \
        fakecamera.cpp \
        genericasicamera.cpp \
        genericatikcamera.cpp \
        main.cpp \
        remotepicamera.cpp \
        rpcmessage.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += /usr/local/include

HEADERS += \
    ASICamera2.h \
    AtikCameras.h \
    abstractcamera.h \
    asi120mmcamera.h \
    atikhorizoniicamera.h \
    commons.h \
    discovery.h \
    fakecamera.h \
    genericasicamera.h \
    genericatikcamera.h \
    remotepicamera.h \
    rpcmessage.h

DISTFILES += \
    remotepicamera.conf

macx: LIBS += -L/usr/local/lib -latikcameras -lASICamera2
