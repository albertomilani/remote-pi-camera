#ifndef RPCCOMMAND_H
#define RPCCOMMAND_H


class RPCMessage
{
public:
    RPCMessage();

private:

    enum MessageType {
        REQUEST,
        REPLY
    };

    enum Command {
        INVENTORY,
        STATUS
    };

    #pragma pack(push, 1)

    struct Message {
        MessageType type;
        Command command;
        int payloadLenght;
        int payloadCrc;
        int headerCrc;
        char *payload;

        Message() {
            payloadLenght = 0;
            payloadCrc = 0;
            headerCrc = 0;
            payload = nullptr;
        }
    };

    #pragma pack(pop)
};

#endif // RPCCOMMAND_H
