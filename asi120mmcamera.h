#ifndef ASI120MMCAMERA_H
#define ASI120MMCAMERA_H

#include "genericasicamera.h"

class Asi120mmCamera : public GenericAsiCamera
{
public:
    Asi120mmCamera(QObject *parent = nullptr);
};

#endif // ASI120MMCAMERA_H
